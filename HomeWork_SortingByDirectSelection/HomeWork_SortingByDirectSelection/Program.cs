﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_SortingByDirectSelection
{
    class Program
    {
        static void Main(string[] args)
        {
            int k = 0, x = 0;
            Console.Write("Введите количество элементов массива: ");
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];
            for (int i = 0; i < n; i++)
            {
                Console.Write("Введите {0}-й элемент массива: ", i + 1);
                array[i] = int.Parse(Console.ReadLine());
            }
            
            for (int i = 0; i < array.Length - 1; i++)
            {
                x = array[i];
                k = i;
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (array[j] < x)
                    {
                        k = j;
                        x = array[j];
                    }                    
                }
                array[k] = array[i];
                array[i] = x;
            }

            for (int i = 0; i < n; i++)
            {
                Console.Write(array[i]);
                Console.Write(' ');
            }
            Console.ReadKey();
        }
    }
}
