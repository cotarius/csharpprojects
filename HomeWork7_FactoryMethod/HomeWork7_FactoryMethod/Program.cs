﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork7_FactoryMethod
{
    interface IButton
    {
        void render();
        void onClick();
    }
    class WindowsButton : IButton
    {
        public void render() { Console.WriteLine("Создана кнопка Windows"); }
        public void onClick() { Console.WriteLine("Нажата кнопка Windows"); }
    }
    class HTMLButton : IButton
    {
        public void render() { Console.WriteLine("Создана HTML кнопка"); }
        public void onClick() { Console.WriteLine("Нажата HTML кнопка"); }                
    }
    abstract class Dialog
    {        
        public void render()
        {
            IButton okButton = createButton();
            okButton.render();
            okButton.onClick();
        }
        public abstract IButton createButton();

    }
    class WindowsDialog : Dialog
    {
        public override IButton createButton() { return new WindowsButton(); }
    }
    class WebDialog : Dialog
    {
        public override IButton createButton() { return new HTMLButton(); }        
    }
    class Application
    {
        public Dialog dialog;
        public void initialize()
        {
            Console.Write("Выедите тип среды: Windows или Web: ");
            string config = Console.ReadLine();
            if (config == "Windows")
            {
                dialog = new WindowsDialog();
            }
            else if (config == "Web")
            {
                dialog = new WebDialog();
            }
            else
                throw new Exception("Error! Unknown Application Type!");
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            Application app = new Application();
            app.initialize();
            app.dialog.render();
            Console.ReadLine();
            //Не смог прицепить метод render() из абстрактного класса Dialog.
            //Возможно в том классе указал неверный возвращаемый тип данных метода render()

        }
    }
}
