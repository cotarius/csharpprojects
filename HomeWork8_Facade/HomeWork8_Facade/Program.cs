﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork8_Facade
{
    class VideoFile
    {
        public string filename;
        public VideoFile(string filename)
        {
            this.filename = filename;
        }
    }
    class OggCompressionCodec
    {
        public void Compression()
        {
            Console.WriteLine("Сработал OggCompressionCodec");
        }
    }
    class MPEG4CompressionCodec
    {
        public void Compression()
        {
            Console.WriteLine("сработал MPEG4CompressionCodec");
        }
    }
    class CodecFactory
    {
        public CodecFactory()
        {

        }
        public void extract(VideoFile file)
        {
            Console.WriteLine("Извлечение...");
        }
    }
    class BitrateReader
    {
        public BitrateReader()
        {

        }
        public void read(string filename, CodecFactory sourceCodec)
        {
            Console.WriteLine("Выделяется Битрейт");
        }
        public void convert(BitrateReader buffer, MPEG4CompressionCodec destinationCodec)
        {
            Console.WriteLine("Конвертация MP4...");
        }
        public void convert(BitrateReader buffer, OggCompressionCodec destinationCodec)
        {
            Console.WriteLine("Конвертация OGG...");
        }
    }
    class AudioMixer
    {
        public BitrateReader fix(BitrateReader result)
        {
            Console.WriteLine("Битрейт фикс...");
            return result;
        }
    }
    class VideoConverter
    {
        VideoFile file;

        public void convert(string filename, string format)
        {
            file = new VideoFile(filename);
            CodecFactory sourceCodec = new CodecFactory();
            sourceCodec.extract(file);
            if (format == "mp4")
            {
                MPEG4CompressionCodec destinationCodec = new MPEG4CompressionCodec();
                destinationCodec.Compression();
            }
            else
            {
                OggCompressionCodec destinationCodec = new OggCompressionCodec();
                destinationCodec.Compression();
            }
            BitrateReader buffer = new BitrateReader();
            buffer.read(filename, sourceCodec);
            BitrateReader result = new BitrateReader();
            result.convert(buffer, destinationCodec);
            var Result = new AudioMixer().fix(result);
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            VideoConverter convertor = new VideoConverter();
            convertor.convert("funny-cats-video.ogg", "mp4");
        }
    }
}
