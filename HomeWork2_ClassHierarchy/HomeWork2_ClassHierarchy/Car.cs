﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork2_ClassHierarchy
{
    class Car : Transport
    {
        private string carModelName;
        private string carColour;

        public string GetCarModuleName() { return carModelName; }
        public string GetCarColour() { return carColour; }
        public void SetCarColour(string carColour) { this.carColour = carColour; }
        public void SetCarModuleName(string carModelName) { this.carModelName = carModelName; }
        public void Print()
        {
            Console.WriteLine("Машина заправлятся: {0}", GetPower());
            Console.WriteLine("Машина едет соскоростью {0} км/ч", GetSpeed());
        }
    }
}
