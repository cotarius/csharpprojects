﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork2_ClassHierarchy
{
    class Baby : Transport
    {
        public void Print()
        {
            Console.WriteLine("Ребёнок питается: {0}", GetPower());
            Console.WriteLine("Ребёнок ползает со скоростью {0} км/ч", GetSpeed());
        }
    }
}
