﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork2_ClassHierarchy
{
    class Train : Transport
    {
        public void Print()
        {
            Console.WriteLine("Поезд заправляется: {0}", GetPower());
            Console.WriteLine("Поезд едет со скоростью {0} км/ч", GetSpeed());
        }
    }
}
