﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork2_ClassHierarchy
{
    class Airplane : Transport
    {
        public void Print()
        {
            Console.WriteLine("Самолёт заправляется: {0}", GetPower());
            Console.WriteLine("Самолёт летает со скоростью {0} км/ч", GetSpeed());
        }
    }
}
