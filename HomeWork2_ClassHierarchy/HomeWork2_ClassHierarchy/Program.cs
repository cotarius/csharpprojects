﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork2_ClassHierarchy
{    
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car();
            car.SetSpeed(60);
            car.SetPower("Бензин");
            car.Print();
            Train train = new Train();
            train.SetSpeed(120);
            train.SetPower("Керосин");
            train.Print();
            Airplane airplane = new Airplane();
            airplane.SetSpeed(800);
            airplane.SetPower("Авиационное топливо");
            airplane.Print();
            Passenger passenger = new Passenger();
            passenger.SetCarModuleName("Ауди");
            passenger.SetCarColour("Белый");
            passenger.SetSpeed(55);
            passenger.Print();
            Baby baby = new Baby();
            baby.SetSpeed(1);
            baby.SetPower("Органическое питание");
            baby.Print();
            Console.ReadKey();
        }
    }
}
