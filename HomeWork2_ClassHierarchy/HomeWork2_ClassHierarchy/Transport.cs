﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork2_ClassHierarchy
{
    class Transport
    {
        private int speed = 0;
        private string power;
        public int GetSpeed() { return speed; }
        public void SetSpeed(int speed) { this.speed = speed; }

        public string GetPower() { return power; }
        public void SetPower(string power) { this.power = power; }
    }
}
