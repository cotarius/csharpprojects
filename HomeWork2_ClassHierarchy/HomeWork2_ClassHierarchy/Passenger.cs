﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork2_ClassHierarchy
{
    class Passenger : Car
    {
        public void Print()
        {
            Console.WriteLine("Пассажир едет в машине {0} {1} цвет со скоростью {2} км/ч", GetCarModuleName(), GetCarColour(), GetSpeed());
        }
    }
}
