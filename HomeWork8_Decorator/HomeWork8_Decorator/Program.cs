﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork8_Decorator
{
    interface DataSource
    {
        void writeData();
        void readData();
    }
    class FileDataSource : DataSource
    {
        private string data;
        public FileDataSource(string data)
        {
            this.data = data;
        }
        public void writeData()
        {
            Console.WriteLine($"Данные {data} записаны");
        }
        public void readData()
        {
            Console.WriteLine($"Данные {data} считаны");
        }
    }
    abstract class DataSourceDecorator : DataSource
    {
        protected DataSource wrappee;
        public DataSourceDecorator(DataSource wrappee)
        {
            this.wrappee = wrappee;
        }
        public virtual void writeData()
        {
            wrappee.writeData();
        }
        public virtual void readData()
        {
            return;
        }
    }
    class EncryptionDecorator : DataSourceDecorator
    {
        public EncryptionDecorator(DataSource wrappee) : base(wrappee) { }
        public override void readData()
        {
            Console.WriteLine("Проведено дешифрование!");
            wrappee.readData();
        }
        public override void writeData()
        {
            wrappee.writeData();
            Console.WriteLine($"Проведено шифрование данных!");
        }
    }
    class CompressionDecorator : DataSourceDecorator
    {
        public CompressionDecorator(DataSource wrappee) : base(wrappee) { }
        public override void readData()
        {
            Console.WriteLine($"Проведена распаковка!");
            wrappee.readData();
        }
        public override void writeData()
        {
            wrappee.writeData();
            Console.WriteLine($"Проведено сжатие данных!");
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            DataSource data1 = new FileDataSource("QWERTY");
            DataSourceDecorator encryption1 = new EncryptionDecorator(data1);
            DataSourceDecorator compression1 = new CompressionDecorator(encryption1);
            compression1.writeData();
            Console.WriteLine();

            DataSource data2 = new FileDataSource("ASDFGH");
            DataSourceDecorator encryption2 = new EncryptionDecorator(data2);
            DataSourceDecorator compression2 = new CompressionDecorator(encryption2);
            compression2.readData();
        }
    }
}
