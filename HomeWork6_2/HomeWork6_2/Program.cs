﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork6_2
{
    interface IEmployee
    {       
        void doWork();
    }
    class Company
    {
        List<IEmployee> employees;
        
        public virtual List<IEmployee> getEmployees()
        {
            throw new NotImplementedException();
        }
        public void createSoftware()
        {
            employees = getEmployees();            
            foreach (var e in employees)
            {
                e.doWork();
            }
        }
    }
    class GameDevCompany : Company
    {
        public override List<IEmployee> getEmployees()
        {
            return new List<IEmployee> { new Designer(), new Artist()};          
        }
    }
    class OutSorsingCompany : Company
    {
        public override List<IEmployee> getEmployees()
        {
            return new List<IEmployee> { new Tester(), new Programmer() };
        }
    }
    class Tester : IEmployee
    {
        public void doWork()
        {
            Console.WriteLine("Тестировщик работает");
        }
    }
    class Programmer : IEmployee
    {
        public void doWork()
        {
            Console.WriteLine("Программист работает");
        }
    }
    class Artist : IEmployee
    {
        public void doWork()
        {
            Console.WriteLine("Артист работает");
        }
    }
    class Designer : IEmployee
    {
        public void doWork()
        {
            Console.WriteLine("Дизайнер работает");
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            GameDevCompany company = new GameDevCompany();
            company.createSoftware();
            OutSorsingCompany company2 = new OutSorsingCompany();
            company2.createSoftware();
        }
    }
}
