﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork7_AbstractFactory
{ 
    interface IButton
    {
        void paint();
    }
    class WinButton : IButton
    {
        public void paint()
        {
            Console.WriteLine("Нарисована кнопка Windows!");
        }
    }
    class MacButton : IButton
    {
        public void paint()
        {
            Console.WriteLine("Нарисована кнопка MacOS!");
        }
    }
    interface ICheckBox
    {
        void paint();
    }
    class WinCheckBox : ICheckBox
    {
        public void paint()
        {
            Console.WriteLine("Отрисован чекбокс в стиле Windows!");
        }
    }
    class MacCheckBox : ICheckBox
    {
        public void paint()
        {
            Console.WriteLine("Отрисован чекбокс в стиле MacOS!");
        }
    }
    interface IGUIFactory
    {
        IButton createButton();
        ICheckBox createCheckBox();
    }
    class WinFactory : IGUIFactory
    {
        public IButton createButton()
        {
            return new WinButton();
        }

        public ICheckBox createCheckBox()
        {
            return new WinCheckBox();
        }
    }
    class MacFactory : IGUIFactory
    {
        public IButton createButton()
        {
            return new MacButton();
        }

        public ICheckBox createCheckBox()
        {
            return new MacCheckBox();
        }
    }
    class Application
    {
        IGUIFactory factory;
        IButton button;
        ICheckBox checkBox;
        public Application(IGUIFactory factory)
        {
            this.factory = factory;
            this.button = factory.createButton();
            button.paint();
            this.checkBox = factory.createCheckBox();
            checkBox.paint();
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            IGUIFactory factory = null;
            Console.WriteLine("Выберите ОС (Windows или Mac)!");
            string config = Console.ReadLine();
            if (config == "Windows")
            {
                factory = new WinFactory();
            }
            else if (config == "Mac")
            {
                factory = new MacFactory();
            }
            else throw new Exception("Error! Unknown operation system!");

            Application app = new Application(factory);
            Console.ReadKey();
        }
    }
}
