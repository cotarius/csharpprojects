﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork9_Observer
{
    interface IEventListener // интерфейс наблюдателя
    {
        void Update(string ev);
    }
    interface IEvent // интерфейс наблюдаемого события
    {
        void AddSubscriber(IEventListener ev);
        void RemoveSubscriber(IEventListener ev);
        void Notify();
    }
    class EventManager : IEvent
    {
        private List<IEventListener> listeners;
        private string someEvent;
        public EventManager(string _event)
        {
            this.someEvent = _event;
            listeners = new List<IEventListener>();
        }
        public void ChangeEvent(string _event)
        {
            this.someEvent = _event;
            Notify();
        }
        public void AddSubscriber(IEventListener _event) ///
        {
            listeners.Add(_event);
            Console.WriteLine("Добавился подписчик " + listeners.Count());
        }
        public void RemoveSubscriber(IEventListener _event)
        {
            listeners.Remove(_event);
            Console.WriteLine($"Подписчик {listeners.Count+1} удален ");
        }
        public void Notify()
        {
            foreach (IEventListener _event in listeners)
                _event.Update(someEvent);
        }
    }
    class Subscriber : IEventListener
    {
        private IEvent _event;
        public Subscriber(IEvent _event)
        {
            this._event = _event;
            _event.AddSubscriber(this);
            this._event.Notify();

        }
        public void Update(string _event)
        {
            Console.WriteLine("Произошла подписка..." + _event);
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            EventManager _event = new EventManager("Событие1");

            Subscriber subscriber1 = new Subscriber(_event);
            _event.ChangeEvent("Событие2");
            _event.ChangeEvent("Событие3");
            _event.RemoveSubscriber(subscriber1);

            Subscriber subscriber2 = new Subscriber(_event);
        }
    }
}
