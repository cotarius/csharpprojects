﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork6_3
{
    public interface IEngine
    {
        void move();
    }
    public interface IDriver
    {
        void navigate();
    }
    class Transport : IDriver, IEngine
    {
        IEngine engine { get; set; }
        IDriver driver { get; set; }

        public void GetEngine(IEngine engine)
        {
            this.engine = engine;
            engine.move();
        }
        public void GetDriver(IDriver driver)
        {
            this.driver = driver;
            driver.navigate();
        }
        public void deliver(string destination, string cargo)
        {
            Console.WriteLine($"Доставка груза {cargo} в пункт назначение {destination}");

        }
        public void move()
        {
            Transport engine = new Transport();
            Console.WriteLine("Выберите тип двигателя для доставки: ");
            Console.WriteLine("1. Обычный");
            Console.WriteLine("2. Электрический");
            int action = Convert.ToInt32(Console.ReadLine());
            switch (action)
            {
                case 1:
                    engine.GetEngine(new CombustionEngine());
                    break;
                case 2:
                    engine.GetEngine(new ElectriсEngine());
                    break;
                default:
                    Console.WriteLine("Ошибка!");
                    break;
            }
        }
        public void navigate()
        {
            Transport driver = new Transport();
            Console.WriteLine("Выберите тип водителя для доставки: ");
            Console.WriteLine("1. Человек");
            Console.WriteLine("2. Робот");
            int action = Convert.ToInt32(Console.ReadLine());
            switch (action)
            {
                case 1:
                    driver.GetDriver(new Human());
                    break;
                case 2:
                    driver.GetDriver(new Robot());
                    break;
                default:
                    Console.WriteLine("Ошибка!");
                    break;
            }
        }
    }
    class CombustionEngine : IEngine
    {
        public void move()
        {
            Console.WriteLine("Машина на бензиновом двигателе");
        }
    }
    class ElectriсEngine : IEngine
    {

        public void move()
        {
            Console.WriteLine("Машина на электрическом двигателе");
        }
    }
    class Robot : IDriver
    {
        public void navigate()
        {
            Console.WriteLine("Водитель - робот Фёдор из Сколково");
        }
    }
    class Human : IDriver
    {
        public void navigate()
        {
            Console.WriteLine("Водитель - Иван Петрович");
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            Transport transport = new Transport();

            transport.deliver("Таганрог", "Апельсины");
            transport.move();
            transport.navigate();
        }
    }
}

