﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork5_3_Overload_Operation_
{
    class Integer
    {        
        public double Value;
        public Integer(double value)
        {
            Value = value;
        }
        public static Integer operator +(Integer x, Integer y)
        {
            return new Integer(x.Value + y.Value);
        }
        public static Integer operator -(Integer x, Integer y)
        {
            return new Integer(x.Value - y.Value);
        }
        public static Integer operator *(Integer x, Integer y)
        {
            return new Integer(x.Value * y.Value);
        }
        public static Integer operator /(Integer x, Integer y)
        {
            return new Integer(x.Value / y.Value);
        }
        public static bool operator ==(Integer x, Integer y)
        {
            return x.Value == y.Value;
        }
        public static bool operator !=(Integer x, Integer y)
        {
            return !(x == y);
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Введите первое натуральное число от 1 до 100:");
                Integer integer1 = new Integer(Convert.ToDouble(Console.ReadLine()));
                if (integer1.Value > 100 || integer1.Value < 0)
                {
                    throw new Exception("Натуральное число число не входит в допустимый диапазон!");
                }
                Console.WriteLine("Введите второе натуральное число от 1 до 100:");
                Integer integer2 = new Integer(Convert.ToDouble(Console.ReadLine()));
                if (integer2.Value > 100 || integer2.Value < 0)
                {
                    throw new Exception("Натуральное число число не входит в допустимый диапазон!");
                }
                Integer integer3 = integer1 + integer2;
                Console.WriteLine("Сумма чисел = {0}", integer3.Value);
                Integer integer4 = integer1 / integer2;
                Console.WriteLine($"Деление чисел = {integer4.Value}");
                if (integer1 == integer2)
                {
                    Console.WriteLine($"Значения {integer1.Value} и {integer2.Value} равны");
                }
                else
                {
                    Console.WriteLine($"Значения {integer1.Value} и {integer2.Value} неравны");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Ошибка! {ex.Message}");
            }
        }
    }
}
