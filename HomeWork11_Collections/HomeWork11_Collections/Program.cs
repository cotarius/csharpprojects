﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace HomeWork11_Collections
{
    class Film
    {
        public string filmName { get; set; }
        public string filmGenre { get; set; }
        public double price { get; set; }

        public void Print()
        {
            Console.WriteLine($"Название: {filmName} \tЖанр: {filmGenre} \tЦена билета: {price}");
            //Console.WriteLine("________________________________________________________________");
        }
    }
    
    internal class Program
    {
        static void Main(string[] args)
        {
            ObservableCollection<Film> films = new ObservableCollection<Film>
            {
                new Film { filmName = "Властелин колец", filmGenre = "Фэнтэзи", price = 450 },
                new Film { filmName = "Матрица", filmGenre = "Фантастика", price = 500 },
                new Film { filmName = "Джентльмены удачи", filmGenre = "Комедия", price = 400 }
            };
            films.CollectionChanged += Films_CollectionChanged;

            foreach (Film film in films)
            {
                film.Print();
            }
            films.Add(new Film { filmName = "Вий", filmGenre = "Ужасы", price = 300 });
            films.RemoveAt(0);
            films[1] = new Film { filmName = "Маша и Медведь", filmGenre = "Мультфильм", price = 350 };

            foreach (Film film in films)
            {
                film.Print();
            }

        }
        private static void Films_CollectionChanged(object sender, NotifyCollectionChangedEventArgs a)
        {
            switch (a.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    Film newFilm = a.NewItems[0] as Film;
                    Console.WriteLine($"Добавлен новый фильм: {newFilm.filmName} \tжанр: {newFilm.filmGenre} \tцена: {newFilm.price}");
                    Console.WriteLine();
                    break;
                case NotifyCollectionChangedAction.Remove:
                    Film oldFilm = a.OldItems[0] as Film;
                    Console.WriteLine($"Закончился прокат фильма: {oldFilm.filmName} \tжанр: {oldFilm.filmGenre} \tцена: {oldFilm.price}");
                    Console.WriteLine();
                    break;
                case NotifyCollectionChangedAction.Replace:
                    Film replacingFilm = a.NewItems[0] as Film;
                    Film replacedFilm = a.OldItems[0] as Film;
                    Console.WriteLine($"Прокат Фильма: {replacedFilm.filmName} \tжанр: {replacedFilm.filmGenre} \tцена: {replacedFilm.price} окончен");
                    Console.WriteLine($"Стартовал прокат нового фильма: {replacingFilm.filmName} \tжанр: {replacingFilm.filmGenre} \tцена: {replacingFilm.price}");
                    Console.WriteLine();
                    break;
            }
        }
        
    }
}
