﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork9_Strategy
{
    interface Strategy
    {
        int Execute(int a, int b);
    }
    class ConcreteStrategyAdd : Strategy
    {
        public int Execute(int a, int b)
        {
            return a + b;
        }
    }
    class ConcreteStrategySubtract : Strategy
    {
        public int Execute(int a, int b)
        {
            return a - b;
        }
    }
    class ConcreteStrategyMultiply : Strategy
    {
        public int Execute(int a, int b)
        {
            return a * b;
        }
    }
    class Context
    {
        Strategy strategy;
        public void setStrategy(Strategy strategy)
        {
            this.strategy = strategy;
        }
        public int executeStrategy(int a, int b)
        {
            return strategy.Execute(a, b);
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            Context context = new Context();
            Console.Write("Введите первое число: ");
            int n1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Введите второе число: ");
            int n2 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Введите знак действия (+, -, *): ");
            char action = Convert.ToChar(Console.ReadLine());
            if (action == '+')
            {
                context.setStrategy(new ConcreteStrategyAdd());
            }
            if (action == '-')
            {
                context.setStrategy(new ConcreteStrategySubtract());
            }
            if (action == '*')
            {
                context.setStrategy(new ConcreteStrategyMultiply());
            }
            int result = context.executeStrategy(n1, n2);
            Console.WriteLine(result);
        }
    }
}

