﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork4_Interfaces
{
    interface IElectricUnit
    {
        void ShowInfo();
    }
    interface IElectricSource : IElectricUnit // источник тока
    {
        int MakeElectricity { get; }       
    }
    interface IElectricAppliance : IElectricUnit// электрический прибор
    {
        int PowerElectricity { get; }       
    }
    interface IElectricWire : IElectricUnit
    {
        int MaxLoad { get; }
        IElectricUnit electric1 { get; }
        IElectricUnit electric2 { get; }
        
    }
    class Kettle : IElectricAppliance
    {
        public int PowerElectricity { get { return 220; } }

        public void ShowInfo()
        {
            Console.WriteLine($"{GetType().Name} потребляет {PowerElectricity}");            
        }
    }
    class Iron : IElectricAppliance
    {
        public int PowerElectricity { get { return 220; } }
        
        public void ShowInfo()
        {
            Console.WriteLine($"{GetType().Name} потребляет {PowerElectricity}");
        }
    }
    class Lathe : IElectricAppliance
    {
        public int PowerElectricity { get { return 380; } }
        
        public void ShowInfo()
        {
            Console.WriteLine($"{GetType().Name} потребляет {PowerElectricity}");
        }
    }
    class Refrigerator : IElectricAppliance
    {
        public int PowerElectricity { get { return 220; } }

        public void ShowInfo()
        {
            Console.WriteLine($"{GetType().Name} потребляет {PowerElectricity}");
        }
    }
    class SolarBattery : IElectricSource
    {
        public int MakeElectricity { get { return 400; } }

        public void ShowInfo()
        {
            Console.WriteLine($"{GetType().Name} потребляет {MakeElectricity}");
        }
    }
    class DiselGenerator : IElectricSource
    {
        public int MakeElectricity { get { return 2000; } }

        public void ShowInfo()
        {
            Console.WriteLine($"{GetType().Name} потребляет {MakeElectricity}");
        }
    }
    class NuclearPowerPlant : IElectricSource
    {
        public int MakeElectricity { get { return 5000000; } }

        public void ShowInfo()
        {
            Console.WriteLine($"{GetType().Name} потребляет {MakeElectricity}");
        }
    }
    class ElectricPowerStrip : IElectricWire
    {
        public int MaxLoad { get { return 400; } }

        public IElectricUnit electric1 { get; set; }

        public IElectricUnit electric2 { get; set; }

        public void ShowInfo()
        {
            Console.WriteLine($"{GetType().Name} потребляет {MaxLoad}");
        }
    }
    class HighLine : IElectricWire
    {
        public int MaxLoad { get { return 2000; } }

        public IElectricUnit electric1 { get; set; }

        public IElectricUnit electric2 { get; set; }

        public void ShowInfo()
        {
            Console.WriteLine($"{GetType().Name} потребляет {MaxLoad}");
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            DiselGenerator diselGenerator = new DiselGenerator();
            Kettle kettle = new Kettle();
            
            ElectricPowerStrip electricPowerStrip = new ElectricPowerStrip();
            electricPowerStrip.electric1 = diselGenerator;
            electricPowerStrip.electric2 = kettle;

            kettle.ShowInfo();
            diselGenerator.ShowInfo();
            
        }
    }
}
