﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork10_Generics
{
    class Cinema<T>
    {
        public string cinemaName { get; set; }
        public T cinemaHall { get; set; }
    }    
    class Film<T> : Cinema<T>
    {
        public string FilmName { get; set; }
        public string FilmGenre { get; set; }
        public int Price { get; set; }       
        
        public void Print()
        {            
            Console.WriteLine($"Кинотеатр: {cinemaName} \tЗал: {cinemaHall}");
            Console.WriteLine($"Фильм: {FilmName}");
            Console.WriteLine($"Жанр: {FilmGenre}");
            Console.WriteLine($"Стоимость билета: {Price}");
            Console.WriteLine();
        }
    }
    internal class Program
    { 
        static void Main(string[] args)
        {
            List<Cinema<string>> cinemaCharlie = new List<Cinema<string>>();
            cinemaCharlie.Add(new Cinema<string> { cinemaName = "Чарли", cinemaHall = "Солнечный"});
            cinemaCharlie.Add(new Cinema<string> { cinemaName = "Чарли", cinemaHall = "Лазурный"});
            List<Film<string>> filmCharlie = new List<Film<string>>();
            filmCharlie.Add(new Film<string> { cinemaName = cinemaCharlie[0].cinemaName, cinemaHall = cinemaCharlie[0].cinemaHall, FilmName = "Властелин Колец", FilmGenre = "Фэнтэзи", Price = 500});
            filmCharlie.Add(new Film<string> { cinemaName = cinemaCharlie[1].cinemaName, cinemaHall = cinemaCharlie[1].cinemaHall, FilmName  = "Звёздные войны", FilmGenre = "Фантастика", Price = 450});

            foreach (Film<string> f in filmCharlie)
            {
                f.Print();               
            }            
            Console.WriteLine("__________________________________________________________________");

            List<Cinema<int>> cinemaNeo = new List<Cinema<int>>();
            cinemaNeo.Add(new Cinema<int> { cinemaName = "Нео", cinemaHall = 1 });
            cinemaNeo.Add(new Cinema<int> { cinemaName = "Нео", cinemaHall = 2 });
            cinemaNeo.Add(new Cinema<int> { cinemaName = "Нео", cinemaHall = 3 });
            List<Film<int>> filmNeo = new List<Film<int>>();
            filmNeo.Add(new Film<int> { cinemaName = cinemaNeo[0].cinemaName, cinemaHall = cinemaNeo[0].cinemaHall, FilmName = "Властелин Колец", FilmGenre = "Фэнтэзи", Price = 550 });
            filmNeo.Add(new Film<int> { cinemaName = cinemaNeo[1].cinemaName, cinemaHall = cinemaNeo[1].cinemaHall, FilmName = "Звёздные войны", FilmGenre = "Фантастика", Price = 400 });
            filmNeo.Add(new Film<int> { cinemaName = cinemaNeo[2].cinemaName, cinemaHall = cinemaNeo[2].cinemaHall, FilmName = "Джентльмены удачи", FilmGenre = "Комедия", Price = 250 });

            foreach (Film<int> f in filmNeo)
            {
                f.Print();                
            }            
        }
    }
}
